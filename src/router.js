import Vue from "vue";
import VueRouter from "vue-router";

import IntroComponent from "./views/IntroComponent.vue";
import AboutComponent from "./views/AboutComponent.vue";
import SkillsComponent from "./views/SkillsComponent.vue";
import SkillsTechnologiesComponent from "./views/SkillsTechnologiesComponent.vue";
import PortfolioComponent from "./views/PortfolioComponent.vue";
import PortfolioProjectsComponent from "./views/PortfolioProjectsComponent.vue";
import ContactComponent from "./views/ContactComponent.vue";

Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    {
      path: "/",
      name: "intro",
      component: IntroComponent,
      menu: {
        title: "Home",
        icon: "home"
      }
    },
    {
      path: "/o-mnie",
      name: "about",
      component: AboutComponent,
      menu: {
        title: "O mnie",
        icon: "about"
      }
    },
    {
      path: "/umiejetnosci",
      name: "skills",
      component: SkillsComponent,
      children: [
        {
          path: "stos-technologiczny",
          name: "skills-technologies",
          component: SkillsTechnologiesComponent,
          menu: {
            title: "Stos technologiczny",
            icon: ""
          }
        }
      ],
      menu: {
        title: "Umiejętności",
        icon: "skills"
      }
    },
    {
      path: "/portfolio",
      name: "portfolio",
      component: PortfolioComponent,
      children: [
        {
          path: "wybrane-projekty",
          name: "portfolio-projects",
          component: PortfolioProjectsComponent,
          menu: {
            title: "Wybrane projekty",
            icon: ""
          }
        }
      ],
      menu: {
        title: "Portfolio",
        icon: "portfolio"
      }
    },
    {
      path: "/kontakt",
      name: "contact",
      component: ContactComponent,
      menu: {
        title: "Kontakt",
        icon: "contact"
      }
    }
  ]
});

export default router;
