import Vue from "vue";
import Vuex from "vuex";

// Modules
import menu from "./modules/menu";
import screen from "./modules/screen";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: { menu, screen }
});
