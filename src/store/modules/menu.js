import homeIcon from "../../assets/img/icons/home.png";
import aboutIcon from "../../assets/img/icons/about.png";
import skillsIcon from "../../assets/img/icons/skills.png";
import portfolioIcon from "../../assets/img/icons/portfolio.png";
import contactIcon from "../../assets/img/icons/contact.png";

const state = {
  menu: {
    isActive: false,
    icons: {
      home: homeIcon,
      about: aboutIcon,
      skills: skillsIcon,
      portfolio: portfolioIcon,
      contact: contactIcon
    }
  }
};

const getters = {
  getActivity(state) {
    return state.menu.isActive;
  },

  getIcons(state) {
    return state.menu.icons;
  }
};

const mutations = {
  changeActivity(state) {
    state.menu.isActive = !state.menu.isActive;
  }
};

const actions = {
  changeActivityCommit({ commit }) {
    commit("changeActivity");
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
